CREATE TABLE public.bankcard
(
    id bigint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1 ),
    bankcardnumber character(50) COLLATE pg_catalog."default" NOT NULL,
    id_person bigint NOT NULL,
    CONSTRAINT bankcard_pkey PRIMARY KEY (id),
    CONSTRAINT "UIBCX" UNIQUE (bankcardnumber),
    CONSTRAINT "FKBCX" FOREIGN KEY (id_person)
        REFERENCES public.person (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE public.bankcard
    OWNER to postgres;