
package com.springjpa.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.springjpa.model.Person;
import com.springjpa.model.Bankcard;
import com.springjpa.repository.PersonRepository;
import com.springjpa.repository.BankcardRepository;
 

@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Service
public class BankcardService {
	 @Autowired
	    private BankcardRepository bankcardRepository;
	 
	    @Autowired
	    private PersonRepository personRepository;
	 
	    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	    public List<Bankcard> getAllBankcards() {
	 
	        return bankcardRepository.findAll();
	    }   
	    
	    
	    public List<Person> getAllPersons() {
	   	 
	        return personRepository.findAll();
	    }   
	    
	    
	    
	    public Bankcard addBankcard(Bankcard bankcard) {
	 
	        Person person = personRepository.findById(bankcard.getPerson().getId()).orElse(null);
	        if (null == person) {
	            person = new Person();
	        }
	        bankcard.setPerson(person);
	        return bankcardRepository.save(bankcard);
	    }
	 
	   

}
