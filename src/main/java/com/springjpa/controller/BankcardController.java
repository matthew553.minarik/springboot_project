

package com.springjpa.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.springjpa.model.Bankcard;
import com.springjpa.model.Person;
import com.springjpa.service.BankcardService;
 
 
@RestController
public class BankcardController {
 
    @Autowired
    private BankcardService bankcardService;
 
        
    @GetMapping("/listbc")
    //@RequestMapping("/list")
    public ResponseEntity<List<Bankcard>> getAllBankcards() {
 
        List<Bankcard> bankcards = bankcardService.getAllBankcards();
    	return new ResponseEntity<>(bankcards, HttpStatus.OK);
    }
 
    @GetMapping("/listperson")
    //@RequestMapping("/list")
    public ResponseEntity<List<Person>> getAllPersons() {
 
        List<Person> persons = bankcardService.getAllPersons();
    	return new ResponseEntity<>(persons, HttpStatus.OK);
    }

    
    
    @PostMapping("/save")
    public ResponseEntity<Bankcard> saveBankcard(@RequestBody Bankcard bankcard) {
 
    	Bankcard bc = bankcardService.addBankcard(bankcard);
        return new ResponseEntity<>(bc, HttpStatus.OK);
    }
 
   
}
