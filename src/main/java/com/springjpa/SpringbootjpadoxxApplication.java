package com.springjpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootjpadoxxApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootjpadoxxApplication.class, args);
	}

}
