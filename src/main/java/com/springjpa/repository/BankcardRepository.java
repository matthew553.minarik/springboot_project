package com.springjpa.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.springjpa.model.Bankcard;


public interface BankcardRepository extends JpaRepository<Bankcard, Integer>{
	

}
