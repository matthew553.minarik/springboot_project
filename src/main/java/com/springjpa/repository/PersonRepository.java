package com.springjpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.springjpa.model.Person;

public interface PersonRepository extends JpaRepository<Person, Integer> {
	 
}
